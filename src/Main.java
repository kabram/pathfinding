import java.util.ArrayList;


import model.Area;
import model.Point;
import model.Robot;

public class Main {

	public static void main(String[] args) {

		ArrayList<Robot> Robots = new ArrayList<Robot>();
		
		Robots.add(new Robot(new Point(20,33),new Point(99,9)));
		Robots.add(new Robot(new Point(90,90),new Point(0,90)));
		Robots.add(new Robot(new Point(0,9),new Point(39,0)));
		Robots.add(new Robot(new Point(90,0),new Point(0,49)));
		Robots.add(new Robot(new Point(4,50),new Point(65,9)));
		Robots.add(new Robot(new Point(2,3),new Point(0,20)));
		Robots.add(new Robot(new Point(90,7),new Point(42,2)));
		Robots.add(new Robot(new Point(1,0),new Point(5,29)));		
		
		
		Area map = new Area(100); 
        new Astar(map,Robots);
        
        for(int k=0;k<Robots.size();k++){
        	int nr = k+1;
        	System.out.println("�CIE�KA BYTU "+nr+". ");
        	System.out.println();
        	printPath(map,Robots.get(k));
        }
	}
	
static void printPath(Area map,Robot Robots){
		
		int[][] tmp = new int[map.NY][map.NX];
			for(int i=0;i<map.NY;i++)
				for(int j=0;j<map.NX;j++)
					for(int l=0;l<Robots.getPath().size();l++)
						if(i == Robots.getPath().get(l).getY() && j == Robots.getPath().get(l).getX())
							tmp[i][j] = 1;
		
		for(int i=0;i<map.NX;i++){
			for(int j=0;j<map.NY;j++)
				if(tmp[i][j]!=0)
					System.out.print(" + ");
				else
					System.out.print(" "+tmp[i][j]+" ");
			System.out.println();
		}
		System.out.println();
		System.out.println();
	}
	
	static void printPaths(Area map,ArrayList<Robot> Robots){
		
		int[][] tmp = new int[map.NY][map.NX];
		for(int k=0;k<Robots.size();k++)
			for(int i=0;i<map.NY;i++)
				for(int j=0;j<map.NX;j++)
					for(int l=0;l<Robots.get(k).getPath().size();l++)
						if(i == Robots.get(k).getPath().get(l).getY() && j == Robots.get(k).getPath().get(l).getX()){
							tmp[i][j] = k+1;
						}
		
		for(int i=0;i<map.NX;i++){
			for(int j=0;j<map.NY;j++)
				System.out.print(" "+tmp[i][j]+" ");
			System.out.println();
			System.out.println();
		}
	}

}
