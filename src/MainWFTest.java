import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import model.Area;
import model.Point;
import model.Robot;

public class MainWFTest {

	private static int N = 8;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		ArrayList<Robot> Robots = new ArrayList<Robot>();

		Robots.add(new Robot(new Point(N-1,N-1),new Point(0,0)));
		Robots.add(new Robot(new Point(0,0),new Point(N-1,N-1)));
		Robots.add(new Robot(new Point(0,N-1),new Point(N-1,0)));
		Robots.add(new Robot(new Point(N-1,0),new Point(0,N-1)));
	
		Area map = new Area(8); 

/*		int[][] map = {{0,0,0,0,0},
						{0,0,0,0,0},
						{0,0,0,0,0},
						{0,1,1,0,0},
						{0,0,1,0,0}};*/
		
//		Area map2 = new Area(map);

		Wavefront w = new Wavefront();
		
		Point [] startPoints = new Point[Robots.size()];
		Point [] goalPoints = new Point[Robots.size()];
		for(int r = 0; r < Robots.size(); r++) {
			startPoints[r] = Robots.get(r).getStart();
			goalPoints[r] = Robots.get(r).getEnd();
		}
		
		w.getWavefrontResult(startPoints, goalPoints, map);
		//printPaths(paths, map2);
}
	
	static void printPaths(HashMap<Integer,Point[]> paths, Area map){
		
		for (Entry<Integer, Point[]> pathEl : paths.entrySet()) {
			Point[] path = pathEl.getValue();
			int[][] tmp = new int[map.NY][map.NX];
			
			for(int y=0; y<map.NY; y++) {
				for(int x=0; x<map.NX; x++)					
					if(map.getTerrainPoint(x, y).isObstacle())
						tmp[y][x] = -1;
					else
						tmp[y][x] = 0;
			}
			
			for(int p=0; p<path.length; p++) {
				tmp[path[p].getY()][path[p].getX()] = 1;
			}
		
			System.out.println("Mapa " + pathEl.getKey());
			for(int y=0; y<map.NY; y++){				
				for(int x=0; x<map.NY; x++)
					if(tmp[y][x] == 0)
						System.out.print(" 0 ");
					else if(tmp[y][x] == 1)
						System.out.print(" + ");
					else
						System.out.print(" X ");
				System.out.println();
			}
			System.out.println();
		}
		
		for (Entry<Integer, Point[]> pathEl : paths.entrySet()) {
			Point[] path = pathEl.getValue();
			System.out.println("Sciezka " + pathEl.getKey());
			for(int p=0; p<path.length; p++) {
				System.out.print(path[p].toString());
				if(p != (path.length - 1))
					System.out.print(" -> ");
			}
			System.out.println();
		}
		
	}


}
