import java.util.HashMap;
import java.util.List;

import model.Area;
import model.Robot;
import model.Point;


public class Astar {
	
	Area[] closedset; //if openset.obstacle = true the point was evaluated
	Area[] openset; // if openset.obstacle = true the point was checked
	HashMap<Point, Point>[] camefrom; // camefrom -> Robot's path
	double[][][] gscore,fscore;
	Point[] current, start, goal;
	
	boolean[] skip;
	boolean[] isThisTheEnd;
	boolean theEnd = false;
	int soClose = 0;
	
	
	// allocating variables and tables, initializing fscore and gscore
	Astar(Area map,List<Robot> rob){
		fscore = new double[rob.size()][map.NX][map.NY];
		gscore = new double[rob.size()][map.NX][map.NY];	
		start = new Point[rob.size()];
		goal = new Point[rob.size()];
		current = new Point[rob.size()];
		closedset = new Area[rob.size()];
		openset = new Area[rob.size()];
		camefrom = new HashMap[rob.size()];
		
		for(int i=0;i<rob.size();i++){
			start[i] = rob.get(i).getStart();
			goal[i] = rob.get(i).getEnd();
			closedset[i] = new Area(map.NX);
			openset[i] = new Area(map.NX);
			camefrom[i] = new HashMap<Point, Point>();
		}
		
		// counting fscore and gscore for each point
		for(int k=0;k<rob.size();k++)
			for(int i=0;i<map.NX;i++)
				for(int j=0;j<map.NY;j++){
					gscore[k][i][j] = estimateCost(start[k],map.getTerrainPoint(i,j)); 
					fscore[k][i][j] = gscore[k][i][j] + estimateCost(start[k],goal[k]);
				}
		findPath(map,rob);	//starting the A* algorithm
	}
	
	// counting the cost using euclidian distance method
	static double estimateCost(Point start, Point end){
		return Math.sqrt(Math.pow(end.getY()-start.getY(),2.0)+Math.pow(end.getX()-start.getX(), 2.0));
	}

	// Finding point in openset with lowest fscore value
	Point getTheNodeFromOpenSetWithTheLowestFscore(int robNr,Area set){
		
		double minFscore = fscore[robNr][goal[robNr].getX()][goal[robNr].getY()];
		Point minFscorePoint = goal[robNr];
		int x=0,y=0;
		for(int i=0;i<set.NY;i++)
			for(int j=0;j<set.NX;j++){
				x = set.getTerrainPoint(i,j).getX();
				y = set.getTerrainPoint(i,j).getY();
				if(set.getTerrainPoint(i,j).getObstacle())
					if(minFscore > fscore[robNr][x][y]){
						minFscore = fscore[robNr][x][y];
						minFscorePoint = set.getTerrainPoint(x,y);
					}
		}
		return minFscorePoint;
	}
	
	// reconstructing found path
	void reconstructPath(Point currentNode, Robot rob,int robNr){
		if(camefrom[robNr].containsKey(currentNode))
			reconstructPath((Point) camefrom[robNr].get(currentNode), rob, robNr);
		rob.appendToPath(currentNode);
		return;
	}
	
	void findPath(Area map,List<Robot> rob){
		
		isThisTheEnd = new boolean[rob.size()];
		skip = new boolean[rob.size()];
		
		for(int k=0;k<rob.size();k++){
			isThisTheEnd[k] = true;
			openset[k].getTerrainPoint(start[k]).setObstacle(true); // add start to openset
		}
		
		skip = areTherePointsInAllOpensets(rob);
		while(!noMorePointsInOpensets(rob)){
			for(int k=0;k<rob.size();k++){
				if(skip[k]==true){
					current[k] = getTheNodeFromOpenSetWithTheLowestFscore(k,openset[k]); // current = the node with lowest fscore value
					if(current[k] == goal[k]){
						for(int r=0; r<rob.size();r++)
							if(k==r && isThisTheEnd[k])
								prepareForTheEnd(k,rob);
						
						if(soClose == rob.size())
							theEnd = true;
						else
							continue;
					}
					openset[k].getTerrainPoint(current[k]).setObstacle(false); // remove current from openset
					closedset[k].getTerrainPoint(current[k]).setObstacle(true); // add current to closedset 
					
					map.getTerrainPoint(current[k].getX(), current[k].getY()).setObstacle(true);
					if(theEnd){
						map.getTerrainPoint(current[k].getX(), current[k].getY()).setObstacle(false);
						break;
					}
				}
			}
		
			if(theEnd){
				findCollisionsAreas(3, rob,map);
				break;
			}
		
			for(int p=0;p<rob.size();p++){
				if(skip[p]==true){
					// which point in neighbourhood should be chosen
					for(Point neighbour: map.findAchievableNeighbors(current[p])){
									
						if(isPointInSet(neighbour,closedset[p])) // if neighbour point is already evaluated skip iteration
							continue;
								
						// if neighbour point isnt evaluated yet or cost...
						double tgscore = gscore[p][current[p].getX()][current[p].getY()] + estimateCost(current[p],neighbour);
						if( !isPointInSet(neighbour,openset[p]) || tgscore < gscore[p][neighbour.getX()][neighbour.getY()]){
							camefrom[p].put(neighbour, current[p]);
							gscore[p][neighbour.getX()][neighbour.getY()] = tgscore; 
							fscore[p][neighbour.getX()][neighbour.getY()] = gscore[p][neighbour.getX()][neighbour.getY()] + estimateCost(neighbour, goal[p]);
							openset[p].getTerrainPoint(neighbour).setObstacle(true);							
						}
					}
				}
			}
			for(int k=0;k<rob.size();k++)
				map.getTerrainPoint(current[k].getX(), current[k].getY()).setObstacle(false);
		}
	}
		
	// looking for collisions in given radius by comparing robots' paths' steps
	// execute Wavefront for found conflicts
	void findCollisionsAreas(int radius,List<Robot> rob,Area map){
		Point begOfCollision = new Point(0,0), endOfCollision;
		int startStep=0, stopStep;
		int collisionLength = 0, xStart, yStart;
		boolean isCollision = false;
		Area collisionArea;
		Point [][] tmp;
		Wavefront WF = new Wavefront();
		Point [] startPoints = new Point[2];
		Point [] goalPoints = new Point[2];
		
		for(int i=0;i<rob.size()-1;i++)
			for(int j=i+1;j<rob.size();j++)
				for(int k=0;k<shorterPathLength(rob.get(i), rob.get(j));k++){
					// if steps are in given radius
					if(Math.abs(rob.get(i).getPath().get(k).getX() - rob.get(j).getPath().get(k).getX())<=radius ||
					   Math.abs(rob.get(i).getPath().get(k).getY() - rob.get(j).getPath().get(k).getY())<=radius){
						if(!isCollision){
							// count the starting point of collision
							begOfCollision = map.getTerrainPoint(rob.get(i).getPath().get(k).getX(),rob.get(i).getPath().get(k).getY());
							/*if(rob.get(i).getStart().equals(begOfCollision))
								startStep = k;
							else
								startStep = k-1;*/
							startPoints[0] = map.getTerrainPoint(rob.get(i).getPath().get(k).getX(),rob.get(i).getPath().get(k).getY());
							startPoints[1] = map.getTerrainPoint(rob.get(j).getPath().get(k).getX(),rob.get(j).getPath().get(k).getY());
							isCollision = true;				
						}

						if(isCollision)
							collisionLength++;	// increment collision length
					}else{
						if(isCollision){
							// count the ending point of collision
							endOfCollision = map.getTerrainPoint(rob.get(i).getPath().get(k).getX(),rob.get(i).getPath().get(k).getY());
							int rozmX = Math.abs(begOfCollision.getX() - endOfCollision.getX())+1;
							int rozmY =  Math.abs(begOfCollision.getY() - endOfCollision.getY())+1;
							
							tmp = new Point[rozmY][rozmX];
							/*if(rob.get(i).getEnd().equals(endOfCollision))
								stopStep = k;
							else
								stopStep = k+1;*/

							goalPoints[0] = map.getTerrainPoint(rob.get(i).getPath().get(k).getX(),rob.get(i).getPath().get(k).getY());
							goalPoints[1] = map.getTerrainPoint(rob.get(j).getPath().get(k).getX(),rob.get(j).getPath().get(k).getY());
							
							xStart = Math.min(begOfCollision.getX(),endOfCollision.getX());
							yStart = Math.min(begOfCollision.getY(),endOfCollision.getY());
							
							// prepare collision area to pass to Wavefront function
							for(int ii=0;ii<rozmX;ii++)
								for(int jj=0;jj<rozmY;jj++){
									tmp[jj][ii] = new Point(ii,jj);
									if(map.getTerrainPoint(xStart + ii, yStart + jj).isObstacle())
										tmp[jj][ii].setObstacle(true); 
									else
										tmp[jj][ii].setObstacle(false); 
								}
							
							collisionArea = new Area(tmp);
							// execute Wavefront function
							// tymczasowy sposob na przeskalowanie + czasowo odjeto drugi byt kolizyjny ze wzgledu na bledne wspolrzedne przekazywane do wf
							Point[] startPoints2 = { new Point(startPoints[0].getX() - xStart,
									startPoints[0].getY() - yStart) };
							Point[] goalPoints2 = { new Point(goalPoints[0].getX() - xStart,
									goalPoints[0].getY() - yStart) };
							
							
							HashMap<Integer, Point[]> wfResultArea = WF.getWavefrontResult(startPoints2, goalPoints2,collisionArea);
							collisionLength = 0;
						}
						isCollision = false;
					}
			}
	}	
	
	// stop condition function
	boolean noMorePointsInOpensets(List<Robot> rob){
		int tmp = 0;
		skip = areTherePointsInAllOpensets(rob);
		for(int k=0;k<rob.size();k++)
			if(skip[k]==true) tmp++;
		if(tmp>0) return false;
		else return true;
	}
	
	// checking if point is in particular set 
	boolean isPointInSet(Point point,Area set){
		return set.getTerrainPoint(point.getX(),point.getY()).getObstacle();
	}
	
	// checking if there are any points left in opensets
	boolean[] areTherePointsInAllOpensets(List<Robot> rob){
		boolean[] howMany = new boolean[rob.size()];
		for(int k=0;k<rob.size();k++)
			if(isNotOpenSetEmpty(openset[k]))
				howMany[k] = true;
			else
				howMany[k] = false;
		return howMany;
	}
	
	// returning shorter path's length
	int shorterPathLength(Robot rob1, Robot rob2){	
		return Math.min(rob1.getPath().size(), rob2.getPath().size());
	}
	
	// checking if there are any points in openset
	boolean isNotOpenSetEmpty(Area openset){
		for(int i=0;i<openset.NY;i++)
			for(int j=0;j<openset.NX;j++)
				if(openset.getTerrainPoint(i,j).getObstacle())
					return true;
		return false;
	}
	
	void prepareForTheEnd(int index,List<Robot> rob){
		reconstructPath(goal[index],rob.get(index),index);
		soClose++;
		isThisTheEnd[index] = false;
		rob.get(index).printPath(index);
	}
		
}