package model.wavefront;

import java.util.LinkedList;
import java.util.List;

import model.Area;
import model.Point;

public class WArea {

	private WPoint [][] map;
	public final int NX, NY;
	
	public WArea(int [][] terrain) {
		NY = terrain.length;
		NX = terrain[0].length; 

		this.map = new WPoint[NY][NX];
		for(int y=0; y<NY; y++)
			for(int x=0; x<NX; x++)
				this.map[y][x] = new WPoint(x, y, terrain[y][x]);
	}
	
	public WArea(Area area) {
		NX = area.NX;
		NY = area.NY;
		
		this.map = new WPoint[NY][NX];
		for(int y=0; y<NY; y++)
			for(int x=0; x<NX; x++)
				this.map[y][x] = new WPoint(area.getTerrainPoint(x, y));		
	}
	
	public WArea(WArea wArea) {
		NX = wArea.NX;
		NY = wArea.NY;
		
		this.map = new WPoint[NY][NX];
		for(int y=0; y<NY; y++)
			for(int x=0; x<NX; x++)
				this.map[y][x] = new WPoint(wArea.getTerrainPoint(x, y));
	}

	public void reset(WArea aCopy) {
		for(int y=0; y<NY; y++)
			for(int x=0; x<NX; x++)
				this.map[y][x] = new WPoint(aCopy.getTerrainPoint(x, y));
	}

	public WPoint getTerrainPoint(int x, int y) {
		return map[y][x];
	}

	public WPoint getTerrainPoint(Point p) {
		return map[p.getY()][p.getX()];
	}
	
	/**
	 * @param p punkt, ktorego sasiadow szukamy
	 * @return sasiadow p po odrzuceniu przeszkod
	 */
	public List<Point> findAchievableNeighborsAsList(Point p) {
		// poza obszarem
		if(p.getX() < 0 || p.getX() >= NX || p.getY() < 0 || p.getY() >= NY)
			return null; // TODO or exception
		
		List<Point> neighbors = new LinkedList<>();
		// lewo
		if(p.getX() > 0 && !map[p.getY()][p.getX()-1].isObstacle())
			neighbors.add(new Point(p.getX() - 1, p.getY()));
		// gora
		if(p.getY() > 0 && !map[p.getY()-1][p.getX()].isObstacle())
			neighbors.add(new Point(p.getX(), p.getY() - 1));
		// prawo
		if(p.getX() < NX - 1 && !map[p.getY()][p.getX()+1].isObstacle())
			neighbors.add(new Point(p.getX() + 1, p.getY()));
		// dol
		if(p.getY() < NY - 1 && !map[p.getY()+1][p.getX()].isObstacle())
			neighbors.add(new Point(p.getX(), p.getY() + 1));

		return neighbors;
	}
	
	// TODO zmienic uwzgledniajac robotId
	public Point findLowerNeighbor(Point p) {
		// poza obszarem
		if(p.getX() < 0 || p.getX() >= NX || p.getY() < 0 || p.getY() >= NY)
			return null; // TODO or exception
		
		int lowerStep = map[p.getY()][p.getX()].getStep() - 1; // TODO: pewnie zmiany w numeracji
		if(lowerStep == 0)
			return null;
		// dol
		if(p.getY() < NY - 1 
				&& !map[p.getY()+1][p.getX()].isObstacle()
				&& map[p.getY()+1][p.getX()].getStep() == lowerStep)
			return (new Point(p.getX(), p.getY() + 1));
		// prawo
		if(p.getX() < NX - 1 
				&& !map[p.getY()][p.getX()+1].isObstacle()
				&& map[p.getY()][p.getX()+1].getStep() == lowerStep)
			return (new Point(p.getX() + 1, p.getY()));
		// gora
		if(p.getY() > 0 
				&& !map[p.getY()-1][p.getX()].isObstacle()
				&& map[p.getY()-1][p.getX()].getStep() == lowerStep)
			return (new Point(p.getX(), p.getY() - 1));
		// lewo
		if(p.getX() > 0 
				&& !map[p.getY()][p.getX()-1].isObstacle()
				&& map[p.getY()][p.getX()-1].getStep() == lowerStep)
			return (new Point(p.getX() - 1, p.getY()));

		return null;
	}
	
	public Point[] getPath(Point goal) {
		int pathLength = map[goal.getY()][goal.getX()].getStep();
		Point[] path = new Point[pathLength];
		
		path[pathLength - 1] = goal;
		for(int i = pathLength - 1; i >= 0; i--) { // TODO -1?
			Point neighbor = findLowerNeighbor(path[i]);
			if(neighbor == null) {
				return path;
			}
			path[i - 1] = neighbor;
		}
		
		return null;
	}
	
	public void print() {
		for(int y=0; y < NY; y++) {
			for(int x=0; x < NX; x++) {
				if(map[y][x].isObstacle()) System.out.print("X "); 
				else System.out.print(map[y][x].getStep() + " ");
				if(map[y][x].getStep() < 10 
						&& map[y][x].getStep() >= 0)
					System.out.print(" ");
			}
			System.out.println();
		}
	}

	public void setStep(Point p, int step) {
		map[p.getY()][p.getX()].setStep(step);
	}

	public int getStep(Point p) {
		return map[p.getY()][p.getX()].getStep();
	}

}
