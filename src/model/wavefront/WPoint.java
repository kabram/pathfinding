package model.wavefront;

import model.Point;

public class WPoint extends Point {

	private int step;		// krok w wf
	
	public WPoint(int x, int y) {
		super(x,y,false);
		this.step = 0;
	}

	public WPoint(int x, int y, int obstacle) {
		super(x,y,obstacle);
		this.step = 0;
	}
	
	public WPoint(Point p) {
		super(p.getX(), p.getY(), p.isObstacle());
		this.step = 0;
	}

	public WPoint(WPoint p) {
		super(p.x, p.y, p.isObstacle());
		this.step = p.step;
	}
	
	public int getStep() {
		return step;
	}
	
	public void setStep(int step) {
		this.step = step; 
	}
}
