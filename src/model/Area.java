package model;

import java.util.LinkedList;
import java.util.List;

import model.Point;

public class Area {

	private Point [][] map;
	public final int NX, NY;
	
	public Area() { 
		NY=NX=10;
	this.map = new Point[NY][NX];
		for(int i=0; i<NX; i++)
			for(int j=0; j<NY; j++)
				this.map[j][i] = new Point(i,j);
	}	
	
	public Area(int size) { 
		NY=NX=size;
	this.map = new Point[NY][NX];
		for(int i=0; i<NX; i++)
			for(int j=0; j<NY; j++)
				this.map[j][i] = new Point(i,j);
	}	
	
	public Area(int [][] terrain) {
		NY = terrain.length;
		NX = terrain[0].length; 

		this.map = new Point[NY][NX];
		for(int y=0; y<NY; y++)
			for(int x=0; x<NX; x++)
				this.map[y][x] = new Point(x, y, terrain[y][x]);
	}
	
	public Area(Point [][] terrain) {
		NY = terrain.length;
		NX = terrain[0].length; 

		this.map = new Point[NY][NX];
		for(int y=0; y<NY; y++)
			for(int x=0; x<NX; x++)
				this.map[y][x] = new Point(terrain[y][x]);
	}
	
	public Area(Area area) {
		NX = area.NX;
		NY = area.NY;
		
		this.map = new Point[NY][NX];
		for(int y=0; y<NY; y++)
			for(int x=0; x<NX; x++)
				this.map[y][x] = new Point(area.map[y][x]);		
	}
		
	public Point getTerrainPoint(int x, int y) {
		return map[y][x];
	}

	public Point getTerrainPoint(Point p) {
		return map[p.getY()][p.getX()];
	}

	public Point findNeighbor(Point p) {
		if(p.getX() < 0 || p.getX() >= NX || p.getY() < 0 || p.getY() >= NY)
			return null; // TODO or exception
		
		if(p.getX() > 0)
			return new Point(p.getX() - 1, p.getY());
		if(p.getY() > 0)
			return new Point(p.getX(), p.getY() - 1);
		if(p.getX() < NX - 1)
			return new Point(p.getX() + 1, p.getY());
		if(p.getY() < NY - 1)
			return new Point(p.getX(), p.getY() + 1);
		
		return null;
	}

	public List<Point> findAchievableNeighbors(Point p) {
		// poza obszarem
		if(p.getX() < 0 || p.getX() >= NX || p.getY() < 0 || p.getY() >= NY)
			return null; // TODO or exception
		
		List<Point> neighbors = new LinkedList<Point>();
		// lewo
		if(p.getX() > 0 && !map[p.getY()][p.getX()-1].getObstacle())
			neighbors.add(new Point(p.getX() - 1, p.getY()));
		// gora
		if(p.getY() > 0 && !map[p.getY()-1][p.getX()].getObstacle())
			neighbors.add(new Point(p.getX(), p.getY() - 1));
		// prawo
		if(p.getX() < NX - 1 && !map[p.getY()][p.getX()+1].getObstacle())
			neighbors.add(new Point(p.getX() + 1, p.getY()));
		// dol
		if(p.getY() < NY - 1 && !map[p.getY()+1][p.getX()].getObstacle())
			neighbors.add(new Point(p.getX(), p.getY() + 1));

		return neighbors;
	}
	
	public void print() {
		System.out.println("AREA");
		
		for(int y=0; y < NY; y++) {
			for(int x=0; x < NX; x++) {
				if(map[y][x].isObstacle()) System.out.print(" X "); 
				else System.out.print(" 0 ");
			}
			System.out.println();
		}
	}
	
}