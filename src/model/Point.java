package model;
public class Point {

	protected int x, y;
	private boolean obstacle;//0 - wolne

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
		this.obstacle = false;
	}

	public Point(int x, int y, boolean obstacle) {
		this.x = x;
		this.y = y;
		this.obstacle = obstacle;
	}
	
	public Point(int x, int y, int obstacle) {
		this.x = x;
		this.y = y;
		if(obstacle == 0)
			this.obstacle = false;
		else 
			this.obstacle = true;
	}
	
	public Point(Point p) {
		this.x = p.x;
		this.y = p.y;
		this.obstacle = p.obstacle;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
            return false;
        if (obj == this)
            return true;
        if (!(obj instanceof Point))
            return false;
		
		Point p = (Point) obj;
		if(this.x == p.x && this.y == p.y)
			return true;
		return false;
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		//return super.hashCode();
		return x*1000 + y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public boolean getObstacle() {
		return obstacle;
	}
	
	public boolean isObstacle() {
		return obstacle;
	}
	
	public void setObstacle(boolean obstacle) {
		if(this.obstacle != obstacle)
			this.obstacle = obstacle;
	}
	
	public String toString() {
		return "(" + x + "," + y + ")";
	}
}
