package model;

import java.util.ArrayList;
import java.util.List;
import model.Point;

public class Robot {

	private Point start;
	private Point end;
	private List<Point> path;
	
	public Robot(Point start, Point end) {
		this.start = start;
		this.end = end;
		this.path = new ArrayList<Point>();
	}
	public Point getStart() {
		return start;
	}
	public void setStart(Point start) {
		this.start = start;
	}
	public Point getEnd() {
		return end;
	}
	public void setEnd(Point end) {
		this.end = end;
	}
	
	public List<Point> getPath(){
		return path;
	}
	
	public void addToPath(Point p){
		this.path.add(p);
	}
	
	public void printPath(){
		System.out.println("D�ugo�� �cie�ki: "+path.size());
		for(int i=0;i<path.size();i++){
			System.out.print("("+path.get(i).getX()+","+path.get(i).getY()+")");
			if(i != (path.size()-1))
				System.out.print(" -> ");
		}
		System.out.println();
	}
	
	public void printPath(int robNr){
		robNr+=1;
		System.out.println("Byt numer: "+robNr);		
		System.out.println("D�ugo�� �cie�ki: "+path.size());
		for(int i=0;i<path.size();i++){
			System.out.print("("+path.get(i).getX()+","+path.get(i).getY()+")");
			if(i != (path.size()-1))
				System.out.print(" -> ");
		}
		System.out.println();
		System.out.println();
	}
	
	public void appendToPath(Point p) {
		this.path.add(p);
	}
	
	public void appendToPath(List<Point> path) {
		for(Point p: path)
			this.path.add(p);
	}
	
	public Point getPathElement(int step) {
		if(step >= path.size())
			return null;
		
		return path.get(step);
	}

}
