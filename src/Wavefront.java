import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import model.Area;
import model.Point;
import model.wavefront.Fringe;
import model.wavefront.WArea;

public class Wavefront {

	// DEF konfliktu - kolizja z innym robotem
	private Area map;
	private WArea [] visited;			// mapa robocza plansz dla kazdego bytu
	private Fringe [] fringes;			// pola w kolejce do sprawdzenia
	
	private Point [] current;
	private int [] completed;			// czy ukonczono algorytm dla poszczegolnych bytow (ogolnie, takze z niepowodzeniem)
		// -1 failure, 0 none, 1 success
	private Point [] goalPoints;
	private HashMap<Integer, Point[]> paths;
	
	private void init(Area map, int robotsCount, Point[] goalPoints) {
		this.map = new Area(map);
		
		this.visited = new WArea[robotsCount];		
		this.fringes = new Fringe[robotsCount];
		this.completed = new int[robotsCount];
		this.current = new Point[robotsCount];
		this.goalPoints = new Point[robotsCount];

		for(int r = 0; r < robotsCount; r++) {
			this.visited[r] = new WArea(map);
			this.fringes[r] = new Fringe();
			this.completed[r] = 0;
			this.goalPoints[r] = goalPoints[r]; 
		}
		
		this.paths = new HashMap<>();
	}
	
	/**
	 * @param startPoints punkty startowe bytow, dla ktorych bedzie wykonywany wf
	 * @param goalPoints punkty koncowe bytow, dla ktorych bedzie wykonywany wf
	 * @param map mapa robocza z zaznaczonymi przeszkodami
	 * @return strukture zawierajaca sciezki dla bytow, dla ktorych bylo to mozliwe - klucz okresla nr bytu zgodny z kolejnoscia w start/goalPoints
	 */
	public HashMap<Integer, Point[]> getWavefrontResult(Point [] startPoints, Point [] goalPoints, Area map) {
		if(startPoints.length < 1)
			return null;
		if(startPoints.length != goalPoints.length)
			return null;
		
		int robotsCount = startPoints.length;
		
		init(map, robotsCount, goalPoints);
		
		System.out.println("======START WAVEFRONT======");
		this.map.print();
		
		// przygotowanie danych przed uruchomieniem WF
		int step = 1;
		for(int r = 0; r < robotsCount; r++) {
			fringes[r].clear();
			fringes[r].add(startPoints[r]);
			visited[r].setStep(fringes[r].get(0), step);
		}

		// WAVEFRONT!
		doWavefront();
		// wypisanie tablic wf
		System.out.println();
		for(int r = 0; r < completed.length; r++) {
			System.out.println("byt " + r);
			visited[r].print();
		}

		// dodanie wyszukanych sciezek
		for(int r = 0; r < completed.length; r++) {
			if(completed[r] > 0)
				paths.put(r, visited[r].getPath(this.goalPoints[r]));
		}
		
		// RERUN
		int loop = 0;
		int countFailed = countWavefrontFailed();
		int newCountFailed;
		boolean reRun = countFailed > 0;
		while(reRun) {
			// przygotowanie: przestawienie niepowodzen na zestawy do wyliczenia
			step = 1;
			for(int r = 0; r < completed.length; r++) {
				this.visited[r] = new WArea(map);
				if(completed[r] == -1) {
					this.map = new Area(map);
					completed[r] = 0;
					fringes[r].clear();
					fringes[r].add(startPoints[r]);
					visited[r].setStep(fringes[r].get(0), step);
				}
			}
			System.out.println("\nReRun " + loop);
			reRunWavefront();
			loop++;
			for(int r = 0; r < completed.length; r++) {
				if(!this.paths.containsKey(r) && completed[r] > 0) {
					Point[] path = visited[r].getPath(this.goalPoints[r]);
					paths.put(r, path);

				}
			}
			
			newCountFailed = countWavefrontFailed();
			if(newCountFailed >= countFailed)
				reRun = false;
			countFailed = newCountFailed;
				
		}
		printMapsAndPaths();
		System.out.println("======END WAVEFRONT======");
		System.out.println();

		return paths;
	}

	private int countWavefrontFailed() {
		int count = 0;
		for(int r = 0; r < completed.length; r++) {			
			if(completed[r] == -1)
				count++;
		}
		
		return count;
	}

	/**
	 * Metoda wykonujaca algorytm wf.
	 * @return pole koncowe algorytmu jesli udalo sie z sukcesem wykonac wf (czyli dojsc do zadanego odcinka koncowego); null wpp
	 */
	private void doWavefront() {
		int step;
		boolean toRemove = true;
		while(isAnyGoalNotAchieved()) {
			
			for(int r = 0; r < fringes.length; r++) {	// pomin jesli zakonczono dla tego bytu
				if(completed[r] != 0)	// nie zakonczono (sukcesem ani porazka)
					continue;
				
				current[r] = fringes[r].get(0);	// pierwszy element listy
				//map.getTerrainPoint(current[r]).setObstacle(false);
				
				if(isGoalAchievedByRobot(r)) {
					completed[r] = 1;
					continue;
				}
				
				step = visited[r].getStep(current[r]) + 1;
				List<Point> children = map.findAchievableNeighbors(current[r]);	// sasiedzi z odjeciem zablokowanych przez OBSTACLE
				
				for(Point child: children) {
					// czy ten punkt juz byl sprawdzany
					if(visited[r].getStep(child) > 0)
						continue;
					if(isBlockedByAnyRobot(r, child, step))
						continue;
					
					fringes[r].add(child);
					visited[r].setStep(child, step);
					//map.getTerrainPoint(child).setObstacle(true);
				}
				
				if(toRemove)
					fringes[r].remove(0);
				else
					toRemove = true;
				setCompletedAndUnblockedIfEmpty(r);
			}
		}
		
	}

	private boolean isBlockedByAnyRobot(int currRobotId, Point child, int step) {
		for(int r = 0; r < visited.length; r++) {
			if(r != currRobotId) {
				if(visited[r].getStep(child) == step
						|| (visited[r].getStep(current[currRobotId]) == step && visited[r].getStep(child) == step - 1))
					return true;
			}
		}
		return false;
	}

	private void reRunWavefront() {
		int step;
		while(isAnyGoalNotAchieved()) {
			
			for(int r = 0; r < fringes.length; r++) {	// pomin jesli zakonczono dla tego bytu
				if(completed[r] != 0)	// nie zakonczono (sukcesem ani porazka)
					continue;
				
				current[r] = fringes[r].get(0);	// pierwszy element listy
				map.getTerrainPoint(current[r]).setObstacle(false);
				
				if(isGoalAchievedByRobot(r)) {
					completed[r] = 1;
					continue;
				}
				
				step = visited[r].getStep(current[r]) + 1;
				List<Point> children = map.findAchievableNeighbors(current[r]);	// sasiedzi z odjeciem zablokowanych przez OBSTACLE
				
/*				System.out.println("\nbyt " + r);
				map.print();
				System.out.println(current[r]);				
				for(Point p: fringes[r])
					System.out.print(p.toString() + " -> ");
				System.out.println();*/
				for(Point child: children) {
					// czy ten punkt juz byl sprawdzany
					if(visited[r].getStep(child) > 0)
						continue;
					if(isInCollisionWithAnyOtherRobot(r, child, step))
						continue;
					if(isBlockedByAnyRobot(r, child, step))
						continue;
					
					fringes[r].add(child);
					visited[r].setStep(child, step);
					map.getTerrainPoint(child).setObstacle(true);
				}
				
				fringes[r].remove(0);
				setCompletedAndUnblockedIfEmpty(r);
			}
		}		
	}

	private boolean isInCollisionWithAnyOtherRobot(int currRobotNo, Point child, int step) {	
		// sprawdzenie kolizji z innym robotem
		for (Entry<Integer, Point[]> path : paths.entrySet()) {
			if(step - 1 < path.getValue().length &&
				((path.getValue()[step - 1].equals(child))
				|| (path.getValue()[step - 2].equals(child) && path.getValue()[step - 1].equals(current[currRobotNo])))
			)
				return true;
		}
		return false;
	}

	private boolean isAnyGoalNotAchieved() {
		for(int r = 0 ; r < completed.length; r++) {
			if(completed[r] == 0)
				return true;
		}
		
		return false;
	}

	private boolean isGoalAchievedByRobot(int r) {
		if(current[r].equals(goalPoints[r]))
			return true;
		return false;
	}

	private void setCompletedAndUnblockedIfEmpty(int robotNo) {
		if(fringes[robotNo].isEmpty()) {
			completed[robotNo] = -1;
			map.getTerrainPoint(current[robotNo]).setObstacle(false);
		}
	}
		
// -------WYPISYWANIE---------
	/**
	 * Wypisanie mapy roboczej.
	 */
	public void print() {
		System.out.println("WAVEFRONT");
		for(int r = 0; r < visited.length; r++)
			visited[r].print();
	}
	
	private void printMapsAndPaths(){
		
		for (Entry<Integer, Point[]> pathEl : paths.entrySet()) {
			Point[] path = pathEl.getValue();
			int[][] tmp = new int[map.NY][map.NX];
			
			for(int y=0; y<map.NY; y++) {
				for(int x=0; x<map.NX; x++)					
					if(map.getTerrainPoint(x, y).isObstacle())
						tmp[y][x] = -1;
					else
						tmp[y][x] = 0;
			}
			
			for(int p=0; p<path.length; p++) {
				tmp[path[p].getY()][path[p].getX()] = 1;
			}
		
			System.out.println("Mapa " + pathEl.getKey());
			for(int y=0; y<map.NY; y++){				
				for(int x=0; x<map.NX; x++)
					if(tmp[y][x] == 0)
						System.out.print(" 0 ");
					else if(tmp[y][x] == 1)
						System.out.print(" + ");
					else
						System.out.print(" X ");
				System.out.println();
			}
			System.out.println();
		}
		
		for (Entry<Integer, Point[]> pathEl : paths.entrySet()) {
			Point[] path = pathEl.getValue();
			System.out.println("Sciezka " + pathEl.getKey());
			for(int p=0; p<path.length; p++) {
				System.out.print(path[p].toString());
				if(p != (path.length - 1))
					System.out.print(" -> ");
			}
			System.out.println();
		}
		
	}
}
